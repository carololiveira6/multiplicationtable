<strong>Entrega 69: Tabela de Multiplicação</strong>
Escreva uma página HTML que use Javascript para desenhar uma tabela de multiplicação de 10x10 no console do navegador. A ideia chave aqui é usar dois loops aninhados (um loop dentro de outro) para costruir um array aninhado (bidimencional) que você vai utilizar para desenhar as linhas e colunas da tabela.

Imagine que você está construindo uma tabela para que professores do ensino fundamental a usem em sala de aula. Ela deve ficar mais ou menos assim:



<strong>Início</strong>

Você pode usar o seguinte documento HTML como ponto de partida:

```<!DOCTYPE html>
<html>
   <head>
       <title>Multiplication Table</title>
   </head>

   <body>
       <h1>Multiplication Table</h1>
       <script>
           // escreve a coluna inicial
            let x = [];
            for (let i = 0; i <= 10; i++) {
                x[i] = [];

            }
            console.table(x)

           /* TODO: escrever dois loops aninhados para desenhar o array bidimencional e monstar o restante da tabela no console.*/
       </script>
   </body>
</html>
```

Se você quiser entender um pouco mais sobre o [console.table()](https://developer.mozilla.org/en-US/docs/Web/API/Console/table) - [Link para documentação](https://developer.mozilla.org/en-US/docs/Web/API/Console/table)


<strong>Bônus (2 pontos máx)</strong>

<em>Para um desafio extra:</em>

Construa uma função que recebe um parâmetro n do tipo inteiro que permita a geração de tabelas de diferentes tamanhos (por exemplo, 12x12, 16x16)
Envio
Faça o push do código para seu repositório no GitLab e use a função do GitLab Pages que permite que o site seja visualizado diretamente. Por favor, envie a url do gitlab pages publicada.